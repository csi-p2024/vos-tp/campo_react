import React, { useState } from 'react';
import { StatusBar, StyleSheet, Text, View } from 'react-native';
import GoalList from './component/GoalList';
import GoalInput from './component/GoalInput';

export default function App() {
  const [sampleGoals, setSampleGoals] = useState([
    "Faire les courses",
    "Aller à la salle 3 fois par semaine",
    "Lire un livre par mois",
    "Apprendre une nouvelle technologie",
    "Manger sainement",
  ]);

  const [editingGoalIndex, setEditingGoalIndex] = useState(null);

  const addGoalHandler = goalTitle => {
    setSampleGoals(currentGoals => [...currentGoals, goalTitle]);
  };

  const removeGoalHandler = goalIndex => {
    setSampleGoals(currentGoals => currentGoals.filter((_, index) => index !== goalIndex));
  };

  const startEditGoalHandler = goalIndex => {
    setEditingGoalIndex(goalIndex);
  };

  const editGoalHandler = (goalIndex, newTitle) => {
    setSampleGoals(currentGoals => {
      const updatedGoals = [...currentGoals];
      updatedGoals[goalIndex] = newTitle;
      return updatedGoals;
    });
    setEditingGoalIndex(null);
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.redText, styles.boldText]}>
        Open up <Text style={styles.boldText}>App.js</Text> to start working on your app!
      </Text>
      <GoalInput
        onAddGoal={addGoalHandler}
        onEditGoal={editGoalHandler}
        editingGoalIndex={editingGoalIndex}
        sampleGoals={sampleGoals}
      />
      <GoalList
        goals={sampleGoals}
        onRemoveGoal={removeGoalHandler}
        onStartEditGoal={startEditGoalHandler}
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  redText: {
    color: '#d9534f',
  },
  boldText: {
    fontWeight: 'bold',
  },
});
