import React, { useState } from 'react';
import { FlatList, Text, Button, View, Modal, StyleSheet, TouchableOpacity } from 'react-native';

const GoalList = ({ goals, onRemoveGoal, onStartEditGoal }) => {
  const [deleteIndex, setDeleteIndex] = useState(null);

  const confirmDelete = () => {
    onRemoveGoal(deleteIndex);
    setDeleteIndex(null);
  };

  return (
    <View style={styles.listContainer}>
      <FlatList
        data={goals}
        renderItem={({ item, index }) => (
          <View style={styles.goalItem}>
            <Text style={styles.goalText}>{item}</Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity onPress={() => onStartEditGoal(index)} style={styles.editButton}>
                <Text style={styles.buttonText}>Edit</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setDeleteIndex(index)} style={styles.removeButton}>
                <Text style={styles.buttonText}>Remove</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
      <Modal visible={deleteIndex !== null} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalText}>Are you sure you want to delete this goal?</Text>
            <View style={styles.modalButtons}>
              <Button title="Cancel" onPress={() => setDeleteIndex(null)} color="#6c757d" />
              <Button title="Confirm" onPress={confirmDelete} color="#d9534f" />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  listContainer: {
    width: '100%',
    paddingHorizontal: 20,
  },
  goalItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
    padding: 15,
    backgroundColor: '#fff',
    borderRadius: 10,
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  goalText: {
    fontSize: 16,
    color: '#333',
    flex: 1,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  editButton: {
    backgroundColor: '#0275d8',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginRight: 5,
  },
  removeButton: {
    backgroundColor: '#d9534f',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 14,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    elevation: 5,
    width: '80%',
    alignItems: 'center',
  },
  modalText: {
    marginBottom: 20,
    fontSize: 16,
  },
  modalButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});

export default GoalList;
