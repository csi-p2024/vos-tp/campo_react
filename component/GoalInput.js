import React, { useState, useEffect } from 'react';
import { Button, TextInput, View, Modal, StyleSheet, TouchableOpacity, Text } from 'react-native';

const GoalInput = ({ onAddGoal, onEditGoal, editingGoalIndex, sampleGoals }) => {
  const [newGoal, setNewGoal] = useState('');
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    if (editingGoalIndex !== null) {
      setNewGoal(sampleGoals[editingGoalIndex]);
      setModalVisible(true);
    }
  }, [editingGoalIndex]);

  const goalInputHandler = enteredText => {
    setNewGoal(enteredText);
  };

  const addGoalHandler = () => {
    onAddGoal(newGoal);
    setNewGoal('');
    setModalVisible(false);
  };

  const editGoalHandler = () => {
    onEditGoal(editingGoalIndex, newGoal);
    setNewGoal('');
    setModalVisible(false);
  };

  const closeModal = () => {
    setNewGoal('');
    setModalVisible(false);
  };

  return (
    <View style={styles.inputContainer}>
      <TouchableOpacity onPress={() => setModalVisible(true)} style={styles.addButton}>
        <Text style={styles.buttonText}>Add Goal</Text>
      </TouchableOpacity>
      <Modal visible={modalVisible} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TextInput
              placeholder="Enter new goal"
              value={newGoal}
              onChangeText={goalInputHandler}
              style={styles.input}
            />
            <View style={styles.modalButtons}>
              <TouchableOpacity onPress={closeModal} style={styles.cancelButton}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={editingGoalIndex !== null ? editGoalHandler : addGoalHandler}
                style={styles.confirmButton}
              >
                <Text style={styles.buttonText}>{editingGoalIndex !== null ? 'Edit' : 'Add'}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  addButton: {
    backgroundColor: '#5cb85c',
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 10,
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    elevation: 5,
    width: '80%',
  },
  input: {
    width: '100%',
    borderColor: 'gray',
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
    marginBottom: 20,
  },
  modalButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cancelButton: {
    backgroundColor: '#6c757d',
    padding: 10,
    borderRadius: 5,
    flex: 1,
    marginRight: 10,
    alignItems: 'center',
  },
  confirmButton: {
    backgroundColor: '#5cb85c',
    padding: 10,
    borderRadius: 5,
    flex: 1,
    alignItems: 'center',
  },
});

export default GoalInput;
